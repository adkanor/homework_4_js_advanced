"use strict";
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Технология AJAX позволяет делать запросы на бэк, обмениваться данными(Jsonом, текстом или html документом)
// Он позволяет без обновления страницы принимать ответы и обрабатывать результаты работы этих запросов.

let list = document.querySelector(".movie_list");
function getUsers() {
  fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((res) => res.json())
    .then((data) => {
      data.forEach((movie) => {
        let movieName = document.createElement("li");
        movieName.classList.add("movie_name");
        movieName.textContent = "Movie Name: " + movie.name;
        let movieEpisode = document.createElement("li");
        movieEpisode.classList.add("movie_episode");
        movieEpisode.textContent = "Movie Episode: " + movie.episodeId;
        let openingCrawl = document.createElement("li");
        openingCrawl.classList.add("movie_openingCrawl");
        openingCrawl.textContent = "Movie Opening Crawl: " + movie.openingCrawl;
        let movieChar = movie.characters.forEach((n) => {
          fetch(n)
            .then((response) => response.json())
            .then((data) => {
              {
                let charName = document.createElement("span");
                charName.classList.add("movie_char");
                charName.textContent = data.name + ", ";
                movieName.after(charName);
              }
            });
        });
        list.append(movieName);
        list.append(movieEpisode);
        list.append(openingCrawl);
      });
    });
}

getUsers();
